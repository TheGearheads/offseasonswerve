import numpy as np

from networktables import NetworkTables

import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from tkinter import Tk, Frame

import sys

import logging

#logging.basicConfig(level=logging.DEBUG)



# Network Tables connection
NetworkTables.initialize(server='roborio-1189-frc.local')

sd = NetworkTables.getTable("SmartDashboard")





class App:

    def __init__(self, master):

        frame = Frame(master)



        # Set up graph

        x = np.arange(0, 20, .1)

        self.LVelocity = np.array([0.0] * len(x))
        self.RVelocity = np.array([0.0] * len(x))
        #self.Position = np.array([0,0] * len(x))
        
        fig = plt.Figure()
 
        ax = fig.add_subplot(111)
        ax2 = ax.twinx()


        self.graph, = ax.plot(x, self.LVelocity)
        self.graph2, = ax2.plot(x, self.RVelocity, 'r')

        ax.set_ylim(2000, 3000)
        ax2.set_ylim(2000,3000)



        self.canvas = FigureCanvasTkAgg(fig, master=master)

        self.canvas.draw()

        self.canvas.get_tk_widget().pack(side='top', fill='both', expand=1)

        frame.pack()

        #self.dataFile = open("C:\src\SwerveBot\dataFile.txt","r+")
        self.hasEnabled = False
        
        self.update()


    def update(self):



        # Update graph data

        self.LVelocity = np.roll(self.LVelocity, -1)
        self.RVelocity = np.roll(self.RVelocity, -1)
        
        #print(sd.getNumber("BL/Distance", 0.0))
        
        #if sd.getBoolean("DS/Enabled",0.0) and sd.getNumber("BL/RVelocity", 0.0) > 0:
            #print("BLLVelocity:", sd.getNumber("BL/LVelocity", 0.0),"BLVolt:",sd.getNumber("BL/RVelocity", 0.0))
            #print("BRLVelocity:", sd.getNumber("BR/LVelocity", 0.0),"BRVolt:",sd.getNumber("BR/RVelocity", 0.0))
            #self.dataFile.write(str(sd.getNumber("BL/LVelocity", 0.0)))
            #self.dataFile.write("\n")
            #self.dataFile.write(str(sd.getNumber("BL/RVelocity", 0.0)))
            #self.dataFile.write("\n")
            #self.hasEnabled = True

        #if self.hasEnabled and not sd.getBoolean("DS/Enabled",0.0):
            #self.dataFile.close()
            #sys.exit(0)
        #if voltMean != oldRVelocity:
        #    print("AVG LVelocity:",LVelocityMean,"AVG Volt:",voltMean)
        #    oldRVelocity = np.mean(self.RVelocity)
        self.LVelocity[-1] = sd.getNumber("BL/RPM", 0.0)
        self.RVelocity[-1] = sd.getNumber("BR/RPM", 0.0)
        
        self.graph.set_ydata(self.LVelocity)
        self.graph2.set_ydata(self.RVelocity)



        # Draw graph

        self.canvas.draw()

        self.canvas.get_tk_widget().after(20, self.update)





root = Tk()

app = App(root)

root.mainloop()
