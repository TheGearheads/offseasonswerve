#pragma once

#include <WPILib.h>
#include <ctre/Phoenix.h>
#include <cmath>
#include <algorithm>
#include "WheelModule.h"
#include "Util.h"

class SwerveDrive {
private:
    std::shared_ptr<WheelModule> backRight;
	std::shared_ptr<WheelModule> backLeft;
	std::shared_ptr<WheelModule> frontRight;
	std::shared_ptr<WheelModule> frontLeft;
    SwerveDrive(SwerveDrive&) = delete;
    SwerveDrive& operator=(const SwerveDrive&) = delete;
public:
    double baseWidth, baseLength;
    double r;
    // 1: frontRight 2: frontLeft 3: backLeft 4: backRight;

    double leftFrontZero, rightFrontZero, leftRearZero, rightRearZero;
    void drive(double x, double y, double rot);
    void drive(double x, double y, double rot, double currentAngle);
    void hold();
    void reset();
    void sendToDash();
    double getDistance();
    double getRPM();
    void resetDistance();
    void setVoltageCompensated(double voltage, bool comp);
    void moveMagic(double dist, int BLid, int BRid, double error);

    SwerveDrive(std::shared_ptr<WheelModule> backRight, std::shared_ptr<WheelModule> backLeft, std::shared_ptr<WheelModule> frontRight, std::shared_ptr<WheelModule> frontLeft);
	//static SwerveDrive* GetInstance();
};