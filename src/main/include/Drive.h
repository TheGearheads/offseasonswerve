#pragma once

#include <WPILib.h>
#include <ctre/Phoenix.h>
#include <cmath>
#include <algorithm>
#include <SwerveDrive.h>
#include "Util.h"
#include <AHRS.h>

class Drive {
    private:
    std::shared_ptr<SwerveDrive> swerve;
    Drive(Drive&) = delete;
    Drive& operator=(const Drive&) = delete;
    bool manualTurn;
    bool zeroing;
    double p;
    double error;
    double offset;
    public:
    bool fieldRel;
    double noDBError;
    Drive();
    AHRS* navx;
    void Init();
    void Periodic();
    void Disabled();
    double getDistance();
    double getRPM();
    void hold();
    double getAngle();
    void resetDistance();
    void setFieldRelative(bool fieldRelative);
    void setVoltageCompensated(double voltage, bool comp);
    void moveMagic(double dist);
    void drive(double x, double y, double rot);
    void drive();
    static Drive& getInstance();
};