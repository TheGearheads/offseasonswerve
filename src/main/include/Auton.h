#pragma once

#include <WPILib.h>
#include <Drive.h>

class Auton {
private:
    Auton();
    Drive& drive;
    std::string autonMode;
	std::string autons[32]; // Enough room to add more autons
    int state;  
public:
	void Init();
	void Periodic();
	void Disabled();
    void Move(double dist, int index);
    void MoveBack(double dist, int index);
    void Turn(double angle, int index);
    void Reset(int index);
    void StrafeLeft(double dist, int index);
    void StrafeRight(double dist, int index);
    void MoveAt45(double dist, int index);
    void TurnAndMove(double dist, int index);
    void setFieldRelative(bool fieldRel, int index);
	static Auton& getInstance() {
		static Auton instance;
		return instance;
	}
};
