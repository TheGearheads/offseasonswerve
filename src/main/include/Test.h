#pragma once

#include <WPILib.h>
#include <Drive.h>

class Test {
private:
    Test();
    Drive& drive;
    Timer voltTimer;
    Timer totalTimer;
    double voltage;
public:
    void Init();
    void Periodic();
    void VoltTest();
    void DriveMin();
    void MotionMagic10Ft();
    static Test& getInstance() {
		static Test instance;
		return instance;
	}    
};