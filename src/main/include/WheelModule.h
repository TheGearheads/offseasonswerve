#pragma once

#include <WPILib.h>
#include <ctre/Phoenix.h>
#include "Util.h"
#include <math.h>
//#include "Robot.h"

class WheelModule {
    private:
        WheelModule(WheelModule&) = delete;
        WheelModule& operator=(const WheelModule&) = delete;
        double f;
    public:
        double setAngle;
        int offset;
        std::shared_ptr<WPI_TalonSRX> angleMotor;
        std::shared_ptr<WPI_TalonSRX> speedMotor;
        WheelModule();
        WheelModule(std::shared_ptr<WPI_TalonSRX> speedMotor, std::shared_ptr<WPI_TalonSRX> angleMotor, int offset, double f, double p);
        void hold();
        void reset();
        double getDistance();
        int getRawDistance();
        int getRawPos();
        double getRawAngle();
        double getAngle();
        int getVelocity();
        double getRPM();
        void resetDistance();
        void setSensorPhase(bool phase);
        void setVoltageCompensated(double voltage, bool comp);
        void sendToDash(std::string name);
        void move(double speed, double angle);
        void moveMagic(double dist, double error);
        void makeFollower(int id);
};

