#include "WheelModule.h"
#include <memory>

WheelModule::WheelModule(std::shared_ptr<WPI_TalonSRX> speedMotor, std::shared_ptr<WPI_TalonSRX> angleMotor, int offset, double f, double p) :
    speedMotor(speedMotor), angleMotor(angleMotor), offset(offset)
 {
    //f = 0;  f value is breaking field relativity
    // when entering field relative, the forward angle is only being set correctly
    // if field relative is off. works fine when setting f to 0.
    angleMotor->Config_kP(0, 6.5, 0);
    angleMotor->Config_kI(0, 0, 0);
    angleMotor->Config_kD(0, 0, 0);
    angleMotor->Config_kF(0, 0, 0);

    this->f = f;
    
    speedMotor->Config_kP(0, p, 0);
    speedMotor->Config_kF(0, f, 0); //f is found by 1023 divided by max clicks/100ms
    speedMotor->ConfigMotionCruiseVelocity(512/f, 0); // Cruise velocity is just max clicks/100ms, so revert back from f value
    speedMotor->ConfigMotionAcceleration(1300, 0);

    

    setAngle = 0;

    angleMotor->ConfigSelectedFeedbackSensor(FeedbackDevice::Analog, 0, 0);
    angleMotor->ConfigSetParameter(ParamEnum::eFeedbackNotContinuous, 0, 0x00, 0x00, 0x00);

    speedMotor->ConfigSelectedFeedbackSensor(FeedbackDevice::QuadEncoder, 0, 0);
}

void WheelModule::hold() {
	speedMotor->Set(0);
    printf("Hold");
}

void WheelModule::reset() {
    double currentAngle = getRawPos();
    int target = offset;
    while (std::abs(currentAngle - target) > 512) {
        if (currentAngle < target) {
            target -= 1024;
        } else {
            target += 1024;
        }
    }
    speedMotor->SetSelectedSensorPosition(0, 0, 0);
    angleMotor->Set(ControlMode::Position, target);

}


double WheelModule::getDistance() {
    return (double)std::abs(speedMotor->GetSelectedSensorPosition(0)) / 510.0;
}

int WheelModule::getRawPos() {
	return angleMotor->GetSelectedSensorPosition(0);
}

double WheelModule::getRawAngle() {
	return (getRawPos() - offset) * (2 * M_PI) / 1024;
}

double WheelModule::getAngle() {
	return normalizeAngle(getRawAngle());
}
int WheelModule::getVelocity() {
    return std::abs(speedMotor->GetSelectedSensorVelocity(0));
}
double WheelModule::getRPM() {
    return (getVelocity() / 80.0) * 600.0;
}
int WheelModule::getRawDistance() {
    return speedMotor->GetSelectedSensorPosition(0);
}
void WheelModule::resetDistance() {
    speedMotor->SetSelectedSensorPosition(0, 0, 0);
}

void WheelModule::setSensorPhase(bool phase) {
    speedMotor->SetSensorPhase(phase);
}


void WheelModule::setVoltageCompensated(double voltage, bool comp) {
    speedMotor->ConfigVoltageCompSaturation(voltage, 0.1);
    speedMotor->EnableVoltageCompensation(true);
    angleMotor->ConfigVoltageCompSaturation(voltage, 0.1);
    angleMotor->EnableVoltageCompensation(true);
}

void WheelModule::sendToDash(std::string name) {
	SmartDashboard::PutNumber(name + "/RawAngle", getRawAngle() * 180 / M_PI);
	SmartDashboard::PutNumber(name + "/RawPos", getRawPos());
	SmartDashboard::PutNumber(name + "/Angle", getAngle() * 180 / M_PI);
	SmartDashboard::PutNumber(name + "/SetpointBeforeOffSet", setAngle - offset);
	SmartDashboard::PutNumber(name + "/Setpoint", setAngle);
    SmartDashboard::PutNumber(name + "/Distance", (double)(speedMotor->GetSelectedSensorPosition(0) / 510.0));
    SmartDashboard::PutNumber(name + "/RawDistance", getRawDistance());
    SmartDashboard::PutNumber(name + "/Velocity", getVelocity());
    SmartDashboard::PutNumber(name + "/RPM", getRPM());
    SmartDashboard::PutNumber(name + "/Voltage", speedMotor->GetMotorOutputVoltage());
}

void WheelModule::moveMagic(double dist, double error) {
    // dist is in feet, convert to clicks
    dist = dist * (558.501); // dist times clicks/foot (4/12) * M_PI * 6.67 * 80
    speedMotor->Set(ControlMode::MotionMagic, dist);
    speedMotor->ConfigMotionCruiseVelocity((512+(std::abs(error)*100))/f, 0);
}

void WheelModule::move(double speed, double angle) {
    speed = -speed;
    angle = normalizeAngle(angle);
    double currentAngle = getRawAngle();
    while (std::abs(currentAngle - angle) > M_PI/2) {
        speed = -speed;
        if (currentAngle < angle) {
            angle -= M_PI;
        } else {
            angle += M_PI;
        }
    }
    setAngle = angle;
    setAngle = setAngle * (1024/(2*M_PI));
    setAngle = setAngle + offset;
	speedMotor->Set(ControlMode::PercentOutput, speed);
	angleMotor->Set(ControlMode::Position, setAngle);
}

void WheelModule::makeFollower(int id) {
    speedMotor->Set(ControlMode::Follower, id);
}