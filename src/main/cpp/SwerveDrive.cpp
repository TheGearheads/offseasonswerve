#include "SwerveDrive.h"

SwerveDrive::SwerveDrive(std::shared_ptr<WheelModule> backRight, std::shared_ptr<WheelModule> backLeft, std::shared_ptr<WheelModule> frontRight, std::shared_ptr<WheelModule> frontLeft):
    backRight(backRight), backLeft(backLeft), frontRight(frontRight), frontLeft(frontLeft) {
    	baseWidth = 22.5;
		baseLength = 20;
		r = sqrt(pow(baseWidth, 2) + pow(baseLength, 2));
        backRight->setSensorPhase(true);
        backLeft->setSensorPhase(true);
        frontRight->setSensorPhase(false);
        frontLeft->setSensorPhase(false);

        backRight->speedMotor->SetInverted(true);
        frontRight->speedMotor->SetInverted(true);
}

void SwerveDrive::hold() {
    backRight->hold();
	backLeft->hold();
	frontRight->hold();
	frontLeft->hold();
}

void SwerveDrive::reset() {
    backRight->reset();
	backLeft->reset();
	frontRight->reset();
	frontLeft->reset();
}

void SwerveDrive::setVoltageCompensated(double voltage, bool comp) {
    backRight->setVoltageCompensated(voltage, comp);
    backLeft->setVoltageCompensated(voltage, comp);
    frontRight->setVoltageCompensated(voltage, comp);
    frontLeft->setVoltageCompensated(voltage, comp);
}

void SwerveDrive::sendToDash() {
    backRight->sendToDash("BR");
	frontRight->sendToDash("FR");
	backLeft->sendToDash("BL");
	frontLeft->sendToDash("FL");
}

double SwerveDrive::getDistance() {
    return ((backRight->getDistance() + backLeft->getDistance()) / 2.0);
}

double SwerveDrive::getRPM() {
    return (backLeft->getRPM() + backRight->getRPM()) / 2;
}

void SwerveDrive::resetDistance() {
    backRight->resetDistance();
    backLeft->resetDistance();
}

void SwerveDrive::moveMagic(double dist, int BLid, int BRid, double error) {
    //backLeft->moveMagic(dist);
    if (error < -1) {
        backLeft->moveMagic(dist, error);
        backRight->moveMagic(dist, 0);
    } else if (error > 1) {
        backRight->moveMagic(dist, error);
        backLeft->moveMagic(dist, 0);
    } else {
        backRight->moveMagic(dist, 0);
        backLeft->moveMagic(dist, 0);
    }
    frontLeft->makeFollower(BLid); 
    frontRight->makeFollower(BRid);
}


void SwerveDrive::drive(double x, double y, double rot) {
    //y = -y;   
    drive(x, y, rot, 0);
}   

void SwerveDrive::drive(double x, double y, double rot, double currentAngle) {
    //y = -y;   
    if (x != 0 || y != 0 || rot != 0) {
        currentAngle = -currentAngle * (M_PI / 180);
        double new_x = x * cos(currentAngle) - y * sin(currentAngle);
        double new_y = x *
        sin(currentAngle) + y * cos(currentAngle);
        x = new_x;
        y = new_y;
        double a = x - rot*(baseLength/r);
        double b = x + rot*(baseLength/r);
        double c = y - rot*(baseWidth/r);
        double d = y + rot*(baseWidth/r);
        
        double targetSpd1 = sqrt(pow(b,2) + pow(d,2));
        double targetSpd2 = sqrt(pow(b,2) + pow(c,2));
        double targetSpd3 = sqrt(pow(a,2) + pow(c,2));
        double targetSpd4 = sqrt(pow(a,2) + pow(d,2));
        double MaxSpd = 0.0;

        if (targetSpd1 > MaxSpd) {
            MaxSpd = targetSpd1;
        }
        if (targetSpd2 > MaxSpd) {
            MaxSpd = targetSpd2;
        }
        if (targetSpd3 > MaxSpd) {
            MaxSpd = targetSpd3;
        }
        if (targetSpd4 > MaxSpd) {
            MaxSpd = targetSpd4;
        }
        
        if (MaxSpd > 1) {
            targetSpd1 = targetSpd1 / MaxSpd;
            targetSpd2 = targetSpd2 / MaxSpd;
            targetSpd3 = targetSpd3 / MaxSpd;
            targetSpd4 = targetSpd4 / MaxSpd;
        }
        
        double targetAng1 = atan2(b,d);
        double targetAng2 = atan2(b,c);
        double targetAng3 = atan2(a,c);
        double targetAng4 = atan2(a,d);
        
        /*double lastAng1 = targetAng1;
        double lastAng2 = targetAng2;
        double lastAng3 = targetAng3;
        double lastAng4 = targetAng4;*/
        currentAngle = (currentAngle * (M_PI/180));

        backRight->move(targetSpd4, targetAng4);
        backLeft->move(targetSpd3, targetAng3);
        frontLeft->move(targetSpd2, targetAng2);
        frontRight->move(targetSpd1, targetAng1);
    } else {
        this->hold();
    }
}