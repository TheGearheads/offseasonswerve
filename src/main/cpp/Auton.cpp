#include "Auton.h"

Auton::Auton() : drive(Drive::getInstance()) {
    autons[0] = "Drive Forward";
    autons[1] = "Drive and Strafe Left";
    autons[2] = "Strafe Left";
    autons[3] = "Move Forward at 45";
    autons[4] = "Turn and Move";
    autons[5] = "MotionMagic";
    autons[6] = "AroundTable";
	SmartDashboard::PutStringArray("Auto List", autons);
	state = 0;

    drive.setVoltageCompensated(10, true);
}

void Auton::Init() {
    state = 0;
    autonMode = SmartDashboard::GetString("Auto Selector","");
    drive.fieldRel = true;
}

void Auton::Periodic() {
    SmartDashboard::PutNumber("Auton/State", state);
    SmartDashboard::PutNumber("Auton/Distance", drive.getDistance());
    SmartDashboard::PutString("Auton/Auton Mode", autonMode);
    SmartDashboard::PutString("Auton/ActualAutonMode", SmartDashboard::GetString("Auto Selector",""));
    if (autonMode == autons[0]) {
        Move(15, 0);
    } else if (autonMode == autons[1]) {
        Move(13, 0);
        Reset(1);
        StrafeLeft(5, 2);
        Reset(3);
    } else if (autonMode == autons[2]) {
        StrafeLeft(5, 0);
    } else if (autonMode == autons[3]){ 
        Turn(45, 0);
        Reset(1);
        Move(13, 2);
        Reset(3);
        StrafeLeft(7, 4);
        Reset(5);
        StrafeRight(7, 6);
        Reset(7);
        Turn(75, 8);
        Reset(9);
        MoveBack(13, 10);
        Reset(11);
        Turn(1, 12);
    } else if (autonMode == autons[4]){
        TurnAndMove(15, 0);
    } else if (autonMode == autons[5]) {
        drive.moveMagic(50.0);
    } else if (autonMode ==autons[6]) {
        Move(13, 0);
        Reset(1);
        StrafeRight(8, 2);
        Reset(3);
        MoveBack(13, 4);
        StrafeLeft(8, 5);
        Reset(6);
    } else {
        Move(1,0);
    }
}

void Auton::Move(double dist, int index) {
    if (state == index){
        drive.drive(0, -0.5, 0);
        if (drive.getDistance() >= dist) {
            drive.hold();
            state++;
        }
    }
}

void Auton::MoveBack(double dist, int index) {
    if (state == index) {
        drive.drive(0, 0.5, 0);
        if (drive.getDistance() >= dist) {
            drive.hold();
            state++;
        }
    }
}

void Auton::Turn(double angle, int index) {
    if (state == index) {
        if (drive.navx->GetAngle() > angle) {
            drive.drive(0, 0, -0.3);
        } else {
            drive.drive(0, 0, 0.3);
        }
        if (std::abs(drive.getAngle() - angle) < 2) {
            drive.hold();
            state++;
        }
    }
}

void Auton::StrafeLeft(double dist, int index) {
    if (state == index) {
        drive.drive(-0.3, 0, 0);
        if (drive.getDistance() >= dist) {
            drive.hold();
            state++;
        }
    }
}

void Auton::StrafeRight(double dist, int index) {
    if (state == index) {
        drive.drive(0.3, 0, 0);
        if (drive.getDistance() > dist) {
            drive.hold();
            state++;
        }
    }
}

void Auton::Reset(int index) {
    if (state == index) {
        drive.resetDistance();
        if (drive.getDistance() <= 0.1) {
            state++;
        }
    }
}

void Auton::TurnAndMove(double dist, int index) {
    if (state == index){
        drive.drive(-
        0.1, -0.3, 0.1);
        if (drive.getDistance() >= dist) {
            drive.hold();
            state++;
        }
    }
}

void Auton::setFieldRelative(bool fieldRel, int index) {
    if (state == index) {
        drive.setFieldRelative(fieldRel);
        state++;
    }
}

void Auton::MoveAt45(double dist, int index) {
    if (state == index) {
        drive.drive(-0.15, -0.15, 0);
        if (drive.getDistance() >= dist) {
            drive.hold();
            state++;
        }
    }
}