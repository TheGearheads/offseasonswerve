#include "Drive.h"

Drive& Drive::getInstance() {
    static Drive instance;
    return instance;
}

Drive::Drive() {
    auto frontLeftDrive = std::make_shared<WPI_TalonSRX>(40);
    auto frontLeftAngle = std::make_shared<WPI_TalonSRX>(31);
    int frontLeftOffset = 450;
    auto frontLeft = std::make_shared<WheelModule>(frontLeftDrive, frontLeftAngle, frontLeftOffset, 0, 0);

    auto frontRightDrive = std::make_shared<WPI_TalonSRX>(50);
    auto frontRightAngle = std::make_shared<WPI_TalonSRX>(45);
    int frontRightOffset = 291;
    auto frontRight = std::make_shared<WheelModule>(frontRightDrive, frontRightAngle, frontRightOffset, 0, 0);

    auto backLeftDrive = std::make_shared<WPI_TalonSRX>(46);
    auto backLeftAngle = std::make_shared<WPI_TalonSRX>(49);
    int backLeftOffset = 526;
    double backLeftF = 1.962276;
    double backLeftP =  0.0275;//0.1646 (calculated) OR 0.095 (trial)
    double backLeftV = 260.9215;
    auto backLeft = std::make_shared<WheelModule>(backLeftDrive, backLeftAngle, backLeftOffset, backLeftF, backLeftP);

    auto backRightDrive = std::make_shared<WPI_TalonSRX>(47);
    auto backRightAngle = std::make_shared<WPI_TalonSRX>(51);
    int backRightOffset = 511;
    double backRightF = 1.8618054;//1.8618054; (calculated)
    double backRightP = 0.0275;//0.1265; (calculated)
    double backRightV = 260.9215;
    auto backRight = std::make_shared<WheelModule>(backRightDrive, backRightAngle, backRightOffset, backRightF, backRightP);


    swerve = std::make_shared<SwerveDrive>(backRight, backLeft, frontRight, frontLeft);
    navx = new AHRS(SPI::Port::kMXP);
    p = 0.02;
    manualTurn = false;
    zeroing = false;
    error = navx->GetAngle() * p;
    fieldRel = false;
    offset = 0;
    noDBError = error;
}

void Drive::Init() {
    swerve->reset();
    navx->Reset();
    offset = 0;
}

void Drive::Periodic() {
    swerve->sendToDash();
    SmartDashboard::PutNumber("NAVX/Angle", navx->GetAngle());
    SmartDashboard::PutNumber("NAVX/error", error);
    SmartDashboard::PutBoolean("Drive/FieldRelative", fieldRel);
}

double Drive::getDistance() {
    swerve->getDistance();
}

void Drive::hold() {
    swerve->hold();
}

double Drive::getAngle() {
    return navx->GetAngle();
}

void Drive::resetDistance() {
    swerve->resetDistance();
}

void Drive::setFieldRelative(bool fieldRelative) {
    fieldRel = fieldRelative;
}

void Drive::setVoltageCompensated(double voltage, bool comp) {
    swerve->setVoltageCompensated(voltage, comp);
}

double Drive::getRPM() {
    swerve->getRPM();
}

void Drive::moveMagic(double dist) {
    swerve->moveMagic(dist, 46, 47, (navx->GetAngle() - offset));
}

void Drive::drive() {
    //if (((std::abs(navx->GetAngle() - offset) * p)) < 0.03) {
        error = 0;
        noDBError = (navx->GetAngle() - offset) * p;
    //} else {
    //    noDBError = (navx->GetAngle() - offset) * p;
    //    error = (navx->GetAngle() - offset) * p;
    //}
    //change if necessary
    /*if (fieldRel) {
        swerve->drive(x,y,rot - error, navx->GetAngle());
    } else {
        swerve->drive(x,y,rot - error);
    }*/
}

void Drive::drive(double x, double y, double rot) {
    SmartDashboard::PutNumber("Drive/Input/X", x);
    SmartDashboard::PutNumber("Drive/Input/Y", y);
    SmartDashboard::PutNumber("Drive/Input/Rot", rot);
    SmartDashboard::PutBoolean("Drive/manualTurn", manualTurn);
    SmartDashboard::PutBoolean("Drive/zeroing", zeroing);
    SmartDashboard::PutNumber("NAVX/NoDbError", noDBError);
    
    bool nextmanualTurn = std::abs(rot) > 0.001;
    if (manualTurn && !nextmanualTurn) {
        zeroing = true;
    }
    manualTurn = nextmanualTurn;


    if(manualTurn || zeroing) {
        error = 0;
    } else {
        //error = Deadband((navx->GetAngle() - offset) * p, 0.01);
        if (((std::abs(navx->GetAngle() - offset) * p)) < 0.03) {
            error = 0;
            noDBError = (navx->GetAngle() - offset) * p;
        } else {
            noDBError = (navx->GetAngle() - offset) * p;
            error = (navx->GetAngle() - offset) * p;
        }
    }

    if (fieldRel) {
        swerve->drive(x,y,rot - error, navx->GetAngle());
    } else {
        swerve->drive(x,y,rot - error);
    }
    
    if(zeroing && std::abs(navx->GetRate()) < Preferences::GetInstance()->GetDouble("momentumThreshold", 0.05)) {
            zeroing = false;
            offset = navx->GetAngle();
    }
}