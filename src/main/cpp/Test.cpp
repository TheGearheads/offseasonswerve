#include "Test.h"

Test::Test() : drive(Drive::getInstance()) {
    drive.setVoltageCompensated(10, true);
    voltTimer.Start();
    totalTimer.Start();
}

void Test::Init() {
    voltTimer.Start();
    voltTimer.Reset();
    totalTimer.Reset();
    voltage = 0;
}

void Test::Periodic() {
    //MotionMagic10Ft();
}

void Test::VoltTest() {
     if (voltTimer.HasPeriodPassed(1)) {
         voltage = voltage + 0.25;
         voltTimer.Reset();
     }
     if (totalTimer.HasPeriodPassed(28)) {
         voltage = 0;
         voltTimer.Stop();
     }
     drive.drive(0, -voltage/10, 0);

}

void Test::DriveMin() {
    drive.drive(0, 0.17843, 0);
}

void Test::MotionMagic10Ft() {
    drive.moveMagic(10.0);
    
}