/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

#include <iostream>

#include <SmartDashboard/SmartDashboard.h>

Robot::Robot() : TimedRobot(), drive(Drive::getInstance()), auton(Auton::getInstance()), test(Test::getInstance()){}

void Robot::RobotInit() {
  Scheduler::GetInstance();
  /*auto camera0 = CameraServer::GetInstance()->StartAutomaticCapture();
		camera0.SetBrightness(40);

		auto camera1 = CameraServer::GetInstance()->StartAutomaticCapture();
		camera1.SetBrightness(40);*/
}

/**
 * This function is called every robot packet, no matter the mode. Use
 * this for items like diagnostics that you want ran during disabled,
 * autonomous, teleoperated and test.
 *
 * <p> This runs after the mode specific periodic functions, but before
 * LiveWindow and SmartDashboard integrated updating.
 */
void Robot::RobotPeriodic() {
  auto& ds = DriverStation::GetInstance();
  SmartDashboard::PutBoolean("DS/Enabled", ds.IsEnabled());
  SmartDashboard::PutBoolean("DS/Disabled", ds.IsDisabled());
  SmartDashboard::PutBoolean("DS/Test", ds.IsTest());
  SmartDashboard::PutBoolean("DS/Auton", ds.IsAutonomous());
  SmartDashboard::PutBoolean("DS/Teleop", ds.IsOperatorControl());

}

/**
 * This autonomous (along with the chooser code above) shows how to select
 * between different autonomous modes using the dashboard. The sendable chooser
 * code works with the Java SmartDashboard. If you prefer the LabVIEW Dashboard,
 * remove all of the chooser code and uncomment the GetString line to get the
 * auto name from the text box below the Gyro.
 *
 * You can add additional auto modes by adding additional comparisons to the
 * if-else structure below with additional strings. If using the SendableChooser
 * make sure to add them to the chooser code above as well.
 */
void Robot::AutonomousInit() {
  drive.Init();
  auton.Init();
}

void Robot::AutonomousPeriodic() {
  drive.Periodic();
  auton.Periodic();
}

void Robot::TeleopInit() {
  drive.Init();
  drive.fieldRel = false;
}

void Robot::TeleopPeriodic() {
  Joystick joystick(0);
  Drive& drive = Drive::getInstance();
  drive.Periodic();
  double x = Deadband(joystick.GetRawAxis(0), 0.1);
  double y = Deadband(joystick.GetRawAxis(1), 0.1);
  double rot = Deadband(joystick.GetRawAxis(4), 0.1);
  double multiplier = 0.6;
  if (joystick.GetRawButton(1)) {
    drive.fieldRel = true;
  }
  if (joystick.GetRawButton(2)) {
    drive.fieldRel = false;
  }
  if (x < 0.1 && x > -0.1 && y < 0.1 && y > -0.1 && rot < 0.1 && rot > -0.1) {
    drive.drive(0, 0, 0);
  } else {
    if (x == 0 && y == 0) {
      drive.drive(x * multiplier, y * multiplier , rot * multiplier);
    } else{
     drive.drive(x * multiplier, y * multiplier, rot * (multiplier * 0.4));
    }
  }
}

void Robot::TestInit() {
  test.Init();
  drive.Init();
}
void Robot::TestPeriodic() {
  test.Periodic();
  drive.Periodic();
}

#ifndef RUNNING_FRC_TESTS
START_ROBOT_CLASS(Robot)
#endif
