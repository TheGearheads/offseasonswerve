import numpy as np

from networktables import NetworkTables

import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from tkinter import Tk, Frame

import sys

import logging

#logging.basicConfig(level=logging.DEBUG)



# Network Tables connection
NetworkTables.initialize(server='roborio-1189-frc.local')

sd = NetworkTables.getTable("SmartDashboard")





class App:

    def __init__(self, master):

        frame = Frame(master)



        # Set up graph

        x = np.arange(0, 20, 0.02)

        self.RPM = np.array([0.0] * len(x))
        self.Voltage = np.array([0.0] * len(x))
        #self.Position = np.array([0,0] * len(x))
        self.Velocity = np.array([0.0] * len(x))
        self.Navx = np.array([0.0] * len(x))

        fig = plt.Figure()
 
        ax = fig.add_subplot(311)
        ax2 = ax.twinx()
        ax3 = fig.add_subplot(312)
        ax4 = fig.add_subplot(313)


        self.graph, = ax.plot(x, self.RPM)
        self.graph2, = ax2.plot(x, self.Voltage, 'r')
        self.graph3, = ax3.plot(self.Voltage, self.RPM, 'o')
        self.graph4, = ax4.plot(x , self.Navx)

        ax.set_ylim(0, 5300)
        ax2.set_ylim(0,13)
        ax3.set_ylim(0,5300)
        ax3.set_xlim(0, 13)
        ax4.set_ylim(-20,20)



        self.canvas = FigureCanvasTkAgg(fig, master=master)

        self.canvas.draw()

        self.canvas.get_tk_widget().pack(side='top', fill='both', expand=1)

        frame.pack()

        #self.dataFile = open("C:\src\SwerveBot\dataFile.txt","r+")
        self.hasEnabled = False
        
        self.update()


    def update(self):



        # Update graph data

        self.RPM   = np.roll(self.RPM, -1)
        self.Voltage = np.roll(self.Voltage, -1)
        self.Navx = np.roll(self.Navx, -1)
        

        print(sd.getNumber("NAVX/Angle", 0.0))
        #print(sd.getNumber("BL/Distance", 0.0))
        rpmMean = np.mean(self.RPM)
        voltMean = np.mean(self.Voltage)
        oldVoltage = 0
        
        if sd.getBoolean("DS/Enabled",0.0) and sd.getNumber("BL/Voltage", 0.0) > 0:
            print("BLRPM:", sd.getNumber("BL/RPM", 0.0),"BLVolt:",sd.getNumber("BL/Voltage", 0.0))
            print("BRRPM:", sd.getNumber("BR/RPM", 0.0),"BRVolt:",sd.getNumber("BR/Voltage", 0.0))
            #self.dataFile.write(str(sd.getNumber("BL/RPM", 0.0)))
            #self.dataFile.write("\n")
            #self.dataFile.write(str(sd.getNumber("BL/Voltage", 0.0)))
            #self.dataFile.write("\n")
            self.hasEnabled = True

        #if self.hasEnabled and not sd.getBoolean("DS/Enabled",0.0):
            #self.dataFile.close()
            #sys.exit(0)
        #if voltMean != oldVoltage:
        #    print("AVG RPM:",rpmMean,"AVG Volt:",voltMean)
        #    oldVoltage = np.mean(self.Voltage)
        self.RPM[-1] = sd.getNumber("BL/RPM", 0.0)
        self.Voltage[-1] = sd.getNumber("BL/Voltage", 0.0)
        self.Navx[-1] = sd.getNumber("NAVX/error", 0.0)
        
        self.graph.set_ydata(self.RPM)
        self.graph2.set_ydata(self.Voltage)
        self.graph3.set_ydata(self.RPM)
        self.graph3.set_xdata(self.Voltage)
        self.graph4.set_ydata(self.Navx)



        # Draw graph

        self.canvas.draw()

        self.canvas.get_tk_widget().after(20, self.update)





root = Tk()

app = App(root)

root.mainloop()
